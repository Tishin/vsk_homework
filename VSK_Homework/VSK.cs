﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;

namespace VSK_Homework
{

    public class StartPage
    {
        [FindsBy(How = How.XPath, Using = "//*/ui-header//*/a[contains(.,'Путешествия')]")]
        public IWebElement MenuTravels { get; set; }
    }

    public class Travels
    {
        [FindsBy(How = How.XPath, Using = "//*/button[contains(.,'Рассчитать и оформить полис')]")]
        public IWebElement Purchase { get; set; }
    }

    public class PurchasePage
    {
        [FindsBy(How = How.XPath, Using = "//*/p[contains(.,'Страна или город')]/..//input")]
        public IWebElement CountryOrCity { get; set; }

        [FindsBy(How = How.XPath, Using = "//*/p[contains(.,'Дата начала')]/../ctrl-dpicker/div/input")]
        public IWebElement DateFrom { get; set; }

        [FindsBy(How = How.XPath, Using = "//*/p[contains(.,'Дата окончания')]/../ctrl-dpicker/div/input")]
        public IWebElement DateTo { get; set; }

        [FindsBy(How = How.XPath, Using = "//*/button[contains(.,'Оформить полис')]")]
        public IWebElement Arrange { get; set; }
    }

    [TestClass]
    public class VSK
    { 
        [TestMethod]
        public void TestMethod()
        {
            //запускаем браузер и переходим на стартовую страницу
            ChromeDriver chrome = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(chrome, new TimeSpan(0, 0, 1,0));
            chrome.Manage().Window.Maximize();
            chrome.Navigate().GoToUrl("https://shop.vsk.ru/");

            //переходим со стартовой страницы на страницу "путешествия" с помощью меню
            StartPage startPage = new StartPage();
            PageFactory.InitElements(chrome,startPage);
            startPage.MenuTravels.Click();

            //переходим на страницу ввода данных о путешествии
            Travels travels = new Travels();
            PageFactory.InitElements(chrome, travels);
            wait.Until(driver => travels.Purchase.Displayed);
            travels.Purchase.Click();

            //вводим страну, дату начала и конца полиса, переходим на страницу оформления полиса
            PurchasePage purchasePage = new PurchasePage();
            PageFactory.InitElements(chrome, purchasePage);
            purchasePage.CountryOrCity.SendKeys("Египет" + Keys.Enter);

            purchasePage.DateFrom.Clear();
            purchasePage.DateFrom.SendKeys(DateTime.Now.AddDays(5).ToString().Split(' ')[0]);
            wait.Until(driver => purchasePage.Arrange.Enabled);

            purchasePage.DateTo.Clear();
            purchasePage.DateTo.SendKeys(DateTime.Now.AddDays(15).ToString().Split(' ')[0]);
            wait.Until(driver => purchasePage.Arrange.Enabled);

            purchasePage.Arrange.Click();

        }
    }
}
